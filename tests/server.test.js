"use strict";

const expect = require("expect");
const request = require("supertest");
const { ObjectID } = require("mongodb");

const { app } = require("../server/server.js");
const { Todo } = require("../server/models/todo.js");
const { User } = require("../server/models/user.js");

const {populateTodos, populateUsers} = require("./seed/seed.js");

beforeEach(populateUsers);
beforeEach(populateTodos);

describe("POST /Todos", () => {

  it("Should work", () => {
    expect(true).toBe(true);
  });

  it ("Should create a new todo", (done) => {
    var text = "Todo text";
    User.find({})
      .then( (users) => {

        request(app) // app adlı express uygulamasını kullanarak bir istemde bulun
          .post("/todos") // bir post istemi gönder
          .set("x-auth", users[0].tokens[0].token) // veritabanındaki ilk kullanıcı bu todonun sahibi olsun.
          .send({text}) // bu posta ilgili verileri gönder
          .expect(201) // http kodu olarak 201 Created gelmesini bekle.
          .expect((res)=>{
            expect(res.body.text).toBe(text);
          })
          .end((err, res)=>{
            if (err){
              done(err);
              return;
            }

            Todo.find()
              .then((todos)=>{
                expect(todos.length).toBe(3); // Örnek data olarak başta iki tane eklemiştik çünkü
                expect(todos[2].text).toBe(text);
                done();
              })
              .catch((err) => {done(err)});
          });

      });
  });

  it ("Should not create a todo with invalid body data", (done) => {
    const text = "";

    User.find({})
      .then( (users) =>{
        request(app)
          .post("/todos")
          .set("x-auth", users[0].tokens[0].token)
          .send({text})
          .expect(400)
          .end((err, res) => {
            if (err) {
              done(err);
              return;
            }

            Todo.find().then((todos) => {
              expect(todos.length).toBe(2); // Örnek data olarak başta iki tane eklemiştik çünkü
              done();
            })
            .catch((err) => done(err));

          });
      });

  });

});

describe("GET /Todos", () => {
  it("Should get all todos", (done) => {
    User.find({})
      .then((users) => {
        request(app)
          .get("/todos")
          .set("x-auth", users[0].tokens[0].token)
          .expect(200)
          .expect((res) => {
            expect(res.body.todos.length).toBe(1);
          })
          .end(done);
      });

  });
});

describe("GET /Todos/:id", () => {

  it("Should return todo doc", (done) => {

    User.find({})
      .then((users) => {
        Todo.find()
          .then((todos) => {

            const id = todos[0]._id.toHexString();

            request(app)
              .get("/Todos/" + id)
              .set("x-auth", users[0].tokens[0].token)
              .expect(200)
              .expect((res) => {
                expect(res.body.todo.text).toBe(todos[0].text);
              })
              .end(done);
        });
      });

  });

  it ("Should return 404 if todo is not found", (done) => {
      const id = new ObjectID().toHexString();

      User.find({})
        .then((users)=>{
          request(app)
            .get("/Todos/" + id)
            .set("x-auth", users[0].tokens[0].token)
            .expect(404)
            .end(done);
        });


    });

    it ("Should return 404 if non-ObjectID is provided", (done) => {
        const id = new ObjectID().toHexString();

        User.find({})
          .then((users)=>{
            request(app)
              .get("/Todos/" + id + "12333")
              .set("x-auth", users[0].tokens[0].token)
              .expect(404)
              .end(done);
          });
      });

});

describe("DELETE /Todos/:id", () => {

  it("Should remove todo", (done) => {

    User.find({})
      .then((users)=> {

        Todo.find()
          .then((todos) => {

            const id = todos[0]._id.toHexString();

            request(app)
              .delete("/Todos/" + id)
              .set("x-auth", users[0].tokens[0].token)
              .expect(200)
              .expect((res) => {
                expect(res.body.todo._id).toBe(id);
              })
              .end((err, res)=>{
                if (err) {
                  done(err);
                  return;
                }

                Todo.findById(id)
                  .then((todo) => {
                    expect(todo).toBeFalsy();
                    done();
                  })
                  .catch((err) => {
                    done(err);
                  });

              });
        });
      });


  });

  it("Should return 404 if todo is not found", (done) => {
    const id = new ObjectID().toHexString();

    User.find({})
      .then((users) => {
        request(app)
          .delete("/Todos/" + id)
          .set("x-auth", users[0].tokens[0].token)
          .expect(404)
          .end(done);
      })


  });

  it("Should return 404 if ObjectID is invalid", (done) => {
    const id = new ObjectID().toHexString();

    User.find({})
      .then((users) => {
        request(app)
          .delete("/Todos/" + id + "123")
          .set("x-auth", users[0].tokens[0].token)
          .expect(404)
          .end(done);
      });
  });

});

describe("PATCH /Todos/:id", () => {
  it("Should update the todo", (done) => {
    const newText = "Aha da bu yeni metin.";

    User.find({})
      .then((users)=>{
        Todo.find({})
          .then((todos) => {

            const id = todos[0]._id.toHexString();

            request(app)
              .patch("/Todos/"+id)
              .set("x-auth", users[0].tokens[0].token)
              .send({text : newText, completed : true})
              .expect(200)
              .expect((res) => {
                expect(res.body.todo.text).toBe(newText);
                expect(res.body.todo.completed).toBe(true);
                // expect(res.body.todo.completedAt).toBeA("number");
                expect(typeof res.body.todo.completedAt).toBe("number");
              })
              .end(done);

          });
      });

  });

  it("Should clear completedAt when todo is not completed", (done) => {
    const newText = "Aha da bu yeni metin.";

    User.find({})
      .then((users)=>{
        Todo.find()
          .then((todos) => {

            const id = todos[0]._id.toHexString();

            request(app)
              .patch("/Todos/"+id)
              .set("x-auth", users[0].tokens[0].token)
              .send({text : newText, completed : false})
              .expect(200)
              .expect((res) => {
                expect(res.body.todo.text).toBe(newText);
                expect(res.body.todo.completed).toBe(false);
                expect(res.body.todo.completedAt).toBeFalsy();
              })
              .end(done);

          });
      });


  });
});

describe("GET /Users/me", () => {

  it("Should return user if authenticated",  (done) => {

    User.find()
      .then((users) => {
        request(app)
          .get("/Users/me")
          .set("x-auth", users[0].tokens[0].token)
          .expect(200)
          .expect( (res) => {
            expect(res.body._id).toBe(users[0]._id.toHexString());
            expect(res.body.email).toBe(users[0].email);
          })
          .end(done);
      });

  });

  it("Should return 401 if not authenticated", (done) => {

    request(app)
      .get("/Users/me")
      .expect(401)
      .expect((res) => {
        expect(res.body).toEqual({});
      })
      .end(done);

  });

});

describe("POST /Users", () => {

  it("Should create user", (done) => {
    request(app)
      .post("/users")
      .send({
        name : "testName",
        email : "test@email.com",
        password : "123456"
      })
      .expect(200)
      .expect((res) => {
        expect(res.headers["x-auth"]).toBeTruthy();
        expect(res.body).toBeTruthy();
        expect(res.body.email).toBe("test@email.com");
      })
      .end((err)=>{
        if (err){
          done(err);
          return;
        }

        User.findOne({email : "test@email.com"})
          .then((user) => {
            expect(user).toBeTruthy(); // user veri tabanına kaydedilmiş olmalıdır.
            expect(user.password).not.toBe("123456"); // hashlenip saklanması gerektiği için farklı olmalıdır.
            done();
          })
          .catch((err) => done(err));
      });
  });

  it("Should return 400 if password length is less then 6", (done) => {
    request(app)
      .post("/users")
      .send({
        name : "testName",
        email : "test@email.com",
        password : "1234"
      })
      .expect(400)
      .end(done);
  });

  it("Should return 400 if email is invalid", (done) => {
    request(app)
      .post("/users")
      .send({
        name : "testName",
        email : "testemail.com",
        password : "123456"
      })
      .expect(400)
      .end(done);
  });

  it("Should not create user if email is in use", (done) => {

    const newUser = new User({
      name : "Yeni Kullanıcı",
      email : "yeni@yeni.com",
      password : "123456"
    });

    newUser.save()
      .then((savedUser) => {
        request(app)
          .post("/users")
          .send(savedUser)
          .expect(400)
          .end(done);
      });

  });

});

describe("POST /users/login", () => {

  it ("Should login user and return auth token with a valide email and a valid password", (done) => {
    User.find({})
      .then((users) => {
        const id = users[1]._id.toHexString();

        request(app)
          .post("/Users/login")
          .send({email : users[1].email, password : "userTwoPass"})
          .expect(200)
          .expect( (res) => {
            expect(res.body._id).toBe(id);
            expect(res.body.email).toBe(users[1].email);
            expect(res.headers["x-auth"]).toBeTruthy();
          })
          .end((err, res) => {
            if (err) { done(err); return; }

            User.findById(id).then((found) => {
              expect(found.toObject().tokens[0]).toMatchObject({
                access : "auth",
                token : res.headers["x-auth"]
              });
              done();
            })
            .catch((err) => done(err));

          });
      });
  });

  it ("Should reject invalid login with a 400 status code", (done) => {

    request(app)
      .post("/Users/login")
      .send({email : "userOne@example1.com", password : "userOnePass44"})
      .expect(400)
      .expect( (res) => {
        expect(res.body).toEqual({});
      })
      .end(done);
  });

});

describe("DELETE /users/me/token", () => {
  it ("Should remove auth token on logout", () => {

    User.find({})
      .then((users) => {
        const id = users[0]._id.toHexString();
        request(app)
          .delete("/users/me/token")
          .set("x-auth", users[0].tokens[0].token)
          .expect(200)
          .end((err, res) => {
            if (err){
              done(err);
              return;
            }

            User.findById(id)
              .then((foundUser) => {
                expect(foundUser.tokens.length).toBe(0);
                done();
              })
              .catch((err) => done(err));

          });
      })
  });
});
