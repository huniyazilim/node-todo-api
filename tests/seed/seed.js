"use strict";

const { Todo } = require("../../server/models/todo.js");
const { User } = require("../../server/models/user.js");
const { ObjectID } = require("mongodb");

const todos = [
  {text : "First todo"},
  {text : "Second todo", completed : true, completedAt : 333}
];

const users = [
  {name : "userOne", email : "userOne@example.com", password:"userOnePass"},
  {name : "userTwo", email : "userTwo@example.com", password:"userTwoPass"}
];

const populateTodos = async function() {
  await Todo.remove({}); // Her seferinde veritabanındaki Todo'ları sil.
  // user id'lerini todolarla eşleştir.
  const users = await User.find({});
  todos[0]._creator = new ObjectID(users[0]._id.toHexString());
  todos[1]._creator = new ObjectID(users[1]._id.toHexString());
  await Todo.insertMany(todos); // Örnek birkaç kayıt ekle.
}

// populateUsers populateTodos metodundan önce çalıştırılmalıdır çünkü önce oluşturulan
// userlar sonradan oluşturulan todalarla eşleştiriliyor.
const populateUsers = async function () {
  await User.remove({}); // Var olan kullanıcıları sil.
  const userOne = new User(users[0]);
  const userTwo = new User(users[1]);
  await userOne.save();
  await userOne.generateAuthToken();
  await userTwo.save();
}

module.exports = {populateTodos, populateUsers}
