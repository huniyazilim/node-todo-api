"use strict";

const { User } = require("../models/user.js");

const authenticate = async (req, res, next) => {
  const token = req.header("x-auth");

  try {
    const user = await User.findByToken(token);

    if (!user){
      throw new Error("User not found"); // eğer bir şekilde user yok ise yine 401 Unauthorized gönder...
    }

    // eğer geçerli bir kullanıcı bulunduysa onu req nesnesine ekle ve devam et.
    req.user = user;
    req.token = token;
    next();

  } catch (err) {
    // Token geçersiz ya da geçerli bir kullanıcı yok ise 401 Unauthorized gönder gitsin...
    res.status(401).send();
  }

}

module.exports = {authenticate};
