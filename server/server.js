"use strict";

require("./config/config.js");

const express = require('express');
const bodyParser = require('body-parser');
const _ = require("lodash");

const { ObjectID } = require("mongodb");

const {mongoose} = require('./db/mongoose.js');
const {authenticate} = require("./middleware/authenticate.js");

const { Todo } = require("./models/todo.js");
const { User } = require("./models/user.js");

const port = process.env.PORT;

const app = express();

// Use the bodyParser json middleware to get the sent data a json object from the request body.
// this middleware gets the data and adds it to req.body as a json object
app.use(bodyParser.json());

app.get("/test", (req, res) => {
  const text = req.query.text || "No text sent!";
  res.send("Returning test response : " + text);
});

app.post("/todos", authenticate, (req, res) => {
  const todo = new Todo({
    text : req.body.text,
    _creator : req.user._id
  });
  todo.save().then((savedTodo) => {
    res.status(201).send(savedTodo);
  }, (err) => {
    res.status(400).send();
  });
});

app.get("/todos", authenticate, (req, res) => {
  Todo.find({ _creator : req.user._id }) // sadece giriş yapmış kullanıcıya ait todoları getir.
    .then((todos) => {
      res.status(200).send({todos});
    }, (err) => {
      res.status(400).send(err);
    })
});

app.get("/todos/:id", authenticate,  (req, res) => {
  // id of todo...
  const { id } = req.params;

  if (!ObjectID.isValid(id)){
    res.status(404).send();
    return;
  }

  Todo.findOne({
    _id : id,
    _creator : req.user._id
  })
    .then((todo) => {
      if (!todo){
        res.status(404).send();
        return;
      }

      res.status(200).send({todo});

    })
    .catch( (err)=> {
      res.status(400).send();
    });

});

app.delete("/todos/:id", authenticate, (req, res) => {
  // id of todo ...
  const { id } = req.params;

  if (!ObjectID.isValid(id)){
    res.status(404).send();
    return;
  }

  Todo.findOneAndRemove({
    _id : id,
    _creator : req.user._id
  })
    .then((todo) => {
      if (!todo){
        res.status(404).send();
        return;
      }
      res.status(200).send({todo});
    })
    .catch((err) => {
      res.status(400).send();
    });

});

app.patch("/todos/:id", authenticate, (req, res) => {
  const { id } = req.params;

  if (!ObjectID.isValid(id)){
    res.status(404).send();
    return;
  }

  // _.pick metodu dizi içinde belirtilen alanlar eğer mevcut iseler body odlı değişkene set eder.
  const body = _.pick(req.body, ["text", "completed"]);

  if (_.isBoolean(body.completed) && body.completed) {
    body.completedAt = new Date().getTime();
  } else {
    body.completed = false;
    body.completedAt = null; // bu haliyle update edilirse, değeri null olduğu için bu alanı ilgili dökümandan silmiş olursun.
  }

  // dökümanı body nesnesinin içindeki alanlara göre güncelle ve dökümanın güncellenmiş halini döndür.
  Todo.findOneAndUpdate({_id: id, _creator : req.user._id}, {$set : body}, {new : true})
    .then((todo) => {
      if (!todo) {
        res.status(404).send();
        return;
      }

      res.send({todo});
    })
    .catch((err)=>{
      res.status(400).send();
    })

});

app.post("/users", async (req, res) => {
  const body = _.pick(req.body, ["name", "email", "password"]);
  const newUser = new User(req.body);

  try {
    await newUser.save();
    const token = await newUser.generateAuthToken();
    res.header("x-auth", token).send(newUser);
  } catch (err) {
    res.status(400).send(err);
  }

});

// Burada authenticate middleware'ini kullan, geçerli bir token yok ise bu
// middleware zaten 401 fırlatacak ve orada duracak yok geçerli bir kullanıcı bulursa
// o kullanıcıyı req nesnesine ekleyip devam edecek ve bu metodu çağırıp kullanıcıyı geri döndürecek.
app.get("/users/me", authenticate, (req, res) => {
  res.send(req.user);
});

app.post("/users/login", (req, res) => {
  const body = _.pick(req.body, ["email","password"]);

  User.findByCredentials(body.email, body.password)
    .then((loggedInUser) => {
      loggedInUser.generateAuthToken().then((token) => {
        res.header("x-auth", token).send(loggedInUser);
      });
    })
    .catch((err) => {
      res.status(400).send(err);
    });

});

app.delete("/users/me/token", authenticate, (req, res) => {
  req.user.removeToken(req.token)
    .then(()=>{
      res.status(200).send();
    })
    .catch((err)=>{
      res.status(400).send(err);
    })
});

const server = app.listen(port, () => {
  console.log(`Sunucu ${port} numaralı port üzerinde dinlemede.`);
});

server.timeout = 10 * 60 * 1000; // 10 dakika...

module.exports = {app}; // sadece express uygulamasını ihraç et.
