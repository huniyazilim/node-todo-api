"use strict";

const validator = require('validator');
const jwt = require("jsonwebtoken");
const _ = require("lodash");
const bcrypt = require('bcryptjs');

const { mongoose } = require("../db/mongoose.js");

const UserSchema = new mongoose.Schema({
  name : {
    type : String
  },
  email : {
    type : String,
    required  : true,
    minlength : 1,
    trim : true,
    unique : true,
    validate : {
      validator : (value) => {
        return validator.isEmail(value);
      },
      message : "{VALUE} geçerli bir mail adresi değildir."
    }
  },
  password : {
    type : String,
    required : true,
    minlength : 6
  },
  tokens : [{
    access : {
      type : String,
      required : true
    },
    token : {
      type : String,
      required : true
    }
  }]
});

// Schema'daki statics nesnesine eklenen metodlar Model Method olarak kullanılabilir.
UserSchema.statics.findByToken = async function (token) {
  const User = this;

  try {
    let decoded = jwt.verify(token, process.env.JWT_SECRET);
    return await User.findOne({
      _id : decoded._id,
      "tokens.token" : token,
      "tokens.access" : "auth"
    });
  } catch (err) {
    throw err;
  }

}

UserSchema.statics.findByCredentials = function (email, password) {
  const User = this;

  return new Promise( function(resolve, reject) {
    User.findOne({email : email})
      .then((userFound) => {
        if (!userFound) {
          reject("No user found!");
          return;
        }
        // Bak bakalım gönderilen sifre doğru mu?
        bcrypt.compare(password, userFound.password, (err, result) => {
          if (result===true) {
            resolve(userFound);
          } else {
            reject("Bad password!");
          }
        });
    });
  });
}

// Mongoose'un toJsON metodunu override ediyoruz...
UserSchema.methods.toJSON = function () {
  const user = this;
  const userObject = user.toObject();
  // sadece aşağıdaki alanları dönder json olarak...
  return _.pick(userObject, ["_id","email", "name"]);
}

// Schema'daki methods nesnesine eklediğin metodlar Instance Method olarak kullanılabiliyor...
UserSchema.methods.generateAuthToken = function() {
  const user = this;
  const newAccess = "auth";
  const newToken = jwt.sign({_id : user._id.toHexString(), newAccess}, process.env.JWT_SECRET).toString();

  return new Promise( function(resolve, reject) {
    // user.tokens.push() bazı mongoose sürücülerinde çalışmıyor imiş, o yüzden concat kullanıldı.
    user.tokens = user.tokens.concat([{access : newAccess, token: newToken}]);

    user.save().then(()=> {
      resolve(newToken);
    });

  });


}

UserSchema.methods.removeToken = function(token) {
  const user = this;
  // update metodundan dönen promisi döndür.
  return user.update({
    $pull : { // tokens dizisi içindeki öğelerden token özelliği yukarıda parametre olarak gönderdiğimiz tokena eş olanları diziden çıkart.
      tokens : {token}
    }
  })
}

// Adı geçen komuttan önce çalıştırılsın, bu fonksiyon "save" komutundan önce çalışacak...
UserSchema.pre("save", function (next) {
  const user = this;

  // user dökümanındaki password alanı değiştirilmiş ise...
  if (user.isModified("password")){
    bcrypt.genSalt(10, (err, salt) => {
      if (err) { throw err }

      bcrypt.hash(user.password, salt, (err, hash) => {
        if (err) { throw err }
        user.password = hash;
        next();
      });

    });
  } else {
    next();
  }

});

const User = mongoose.model("User", UserSchema);

module.exports = { User };
