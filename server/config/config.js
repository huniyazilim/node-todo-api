"use strict";

// NODE_ENV çevre değişkeni normalde Heroku tarafından sağlanır, lokalde yoktur.
// Lokalde bu değişkeni oluşturmak için package.json dosyası kullanılır.
// test çalıştırmak için test scripti içinde SET \"NODE_ENV=test\"
const env = process.env.NODE_ENV || "development";  // Eğer Heroku üzerinde değilsek varsayılan olarak "development" kullan.

console.log("env ****", env);

if (env === "development" || env === "test") {
  const config = require("./config.json");
  const envConfig = config[env]; // json dosyasından mevcut çevreye ait olan bilgileri alıyoruz.
  Object.keys(envConfig).forEach( (key) => {
    process.env[key] = envConfig[key]; // envConfig nesenesindeki tüm alanları process.env olarak set ediyoruz.
  });
}

if (env == "production") {
  // Bu iki sabit kullanılacak üretim alanına çevre değişkeni olarak sağlanmalıdır.
  const dbUser = process.env.dbUser;
  const dbPass = process.env.dbPass;
  const dbName = process.env.dbName;
  process.env.MONGODB_URI = `mongodb://${dbUser}:${dbPass}@ds121299.mlab.com:21299/${dbName}`;
}
