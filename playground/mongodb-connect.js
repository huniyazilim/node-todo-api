"use strict";

const { MongoClient, ObjectID } = require("mongodb");

MongoClient.connect("mongodb://localhost:27017/TodoApp", (err, db) => {
  if (err){
    console.log("MongoDb veri tabanına bağlanılamadı. ", err.toString());
    return;
  }

  console.log("MongoDb veri tabanına bağlantı sağlandı.");

  const obj = new ObjectID();
  console.log("New ObjectID : " , obj);

  db.collection("Todos").insertOne({
    text : "Yap bir güzellik",
    completed : false
    }, (err, result) => {
    if (err){
      console.log("Todo kaydı eklenemedi", err);
      return;
    }
    console.log("insertedId : " + result.insertedId);
    console.log("insertedCount : " + result.insertedCount);
    console.log(JSON.stringify(result.ops, undefined, 2));
  });

  db.collection("Users").insertOne({
      name : "Nadir",
      age : 39,
      location : "İstanbul"
    }, (err, result) => {
      if (err){
        console.log("Kullanıcı eklenemedi.", err);
        return;
      }
      console.log("Kullanıcı başarıyla eklendi.");
      console.log(JSON.stringify(result.ops, undefined, 2));
      console.log((result.ops[0]._id.getTimestamp()));
    });

  db.close();
});
