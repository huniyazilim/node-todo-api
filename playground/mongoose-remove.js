"use strict";

const { ObjectID } = require("mongodb");

const { mongoose } = require("../server/db/mongoose.js");
const { Todo } = require("../server/models/todo.js");
const { User } = require("../server/models/user.js");

const id = "5acf6573d413d2943159cc1e";
const userId = "5acf0adf917528bc2f0e4538";

// ObjectID.isValid metodunu kullanarak geçersiz idleri yakalayabiliriz.
if (!ObjectID.isValid(userId)){
  console.log("User id is not valid");
}

// Koşula uyan tüm dökümanları siler.
// Todo.remove({}).then((result)=>{
//   console.log(result);
// });

// Todo.findOneAndRemove()
// Todo.findByIdAndRemove()

Todo.findByIdAndRemove("5ad06782a560cf4404804caa")
  .then((todo) => console.log(todo));
