"use strict";

const { MongoClient, ObjectID } = require("mongodb");

MongoClient.connect("mongodb://localhost:27017/TodoApp", (err, db) => {
  if (err){
    console.log("MongoDb veri tabanına bağlanılamadı. ", err.toString());
    return;
  }
  console.log("MongoDb veri tabanına bağlantı sağlandı.");

  // find({koşullar}) : Kolleksiyondaki tüm verileri içeren bir cursor döndürür.
  // toArray : Döküman dizisi içeren bir promise döndürür.

  // ObjectID'ye göre sorgulama yapmak için aşağıdaki yöntem kullanılmalıdır.
  // find({_id : new ObjectID("5acdf76f7371f540e866d95b")})

  db.collection("Todos").find().count().then((count) => {
    console.log("Yapılacakların sayısı : " + count);
  });

  db.collection("Todos").find({completed:false}).toArray().then((docs) => {
    console.log(JSON.stringify(docs, undefined, 2));
  }, (err) => {
    console.log("Yapılacaklar listesi alınamadı.", err);
  });

  db.collection("Users").find({name : "Nadir"}).toArray()
    .then((users) => {
      console.log(JSON.stringify(users, undefined, 2));
    });

  // db.close();
});
