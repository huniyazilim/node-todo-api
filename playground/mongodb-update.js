"use strict";

const { MongoClient, ObjectID } = require("mongodb");

MongoClient.connect("mongodb://localhost:27017/TodoApp", (err, db) => {
  if (err){
    console.log("MongoDb veri tabanına bağlanılamadı. ", err.toString());
    return;
  }
  console.log("MongoDb veri tabanına bağlantı sağlandı.");

  // findOneAndUpdate
  db.collection("Todos")
    .findOneAndUpdate(
      {_id : new ObjectID("5ace55741758d3272c972c07")},
      {
        $set : { // set update işleci ile değeri olduğu gibi değiştiriyoruz.
          completed : true
        }
      },
      {
        returnOriginal : false // güncellenmiş halini döndür.
      }
    )
    .then( (result ) => {
      console.log(result);
    });

  db.collection("Users").findOneAndUpdate({_id : new ObjectID("5ace55741758d3272c972c08")},
    {
      $set : { name : "Nazan"},
      $inc : { age : 1}
    },
    {returnOriginal : false}
  ).then((result)=> {
    const {lastErrorObject} = result;
    if (lastErrorObject.n == 1){
      console.log(result.value);
    } else {
      console.log("Güncelleme gerçekleşmedi.");
    }

  });


  // db.close();
});
