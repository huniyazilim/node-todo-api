"use strict";

const { ObjectID } = require("mongodb");

const { mongoose } = require("../server/db/mongoose.js");
const { Todo } = require("../server/models/todo.js");
const { User } = require("../server/models/user.js");

const id = "5acf6573d413d2943159cc1e";
const userId = "5acf0adf917528bc2f0e4538";

// ObjectID.isValid metodunu kullanarak geçersiz idleri yakalayabiliriz.
if (!ObjectID.isValid(userId)){
  console.log("User id is not valid");
}

// Bir dizi döndürür her seferinde.
Todo.find({
  _id : id // Mongoose için _id parametresine ObjectID göndermeye gerek yok, bunu arka planda kendi hallediyor...
}).then((todos) => {
  console.log("Todos", todos);
});

// Şarta uyan ilk dökumanı döndürür ya da birşey bulamaz ise null döndürür.
// Dolayısıyla birşey bulamadığında boş bir dizi döndüren find metodundansa bu metod tercih edilmelidir.
Todo.findOne({
  _id : id // Mongoose için _id parametresine ObjectID göndermeye gerek yok, bunu arka planda kendi hallediyor...
}).then((todo) => {
  if (!todo){
    console.log("Todo not found!");
    return;
  }
  console.log("Todo", todo);
});

Todo.findById(id)
  .then((todo) => {
    if (!todo){
      console.log("ID not found!");
      return;
    }
    console.log("Todo by Id", todo);
  })
  .catch((err) => console.log(err));

User.findById(userId)
  .then((user) => {
    if (!user){
      console.log("User not found!");
    } else {
      console.log("User ", user);
    }
  }).catch((err) => console.log(err));
