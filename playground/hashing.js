"use strict";

const {SHA256} = require("crypto-js");

const jwt = require("jsonwebtoken");
const bcrypt = require('bcryptjs');

const message = "I am user number 3";
const hash = SHA256(message);

// console.log(message + " => " + hash);

const data = {
  id : 10
}

const token = jwt.sign(data, "123abc");
console.log(token);

const decoded = jwt.verify(token,"123abc");
console.log(decoded);

const password = "123abc!";
let hashedPassword = "$2a$10$ck/LHxMn1p9Zx3bFM/wg0ufEQ2F1plLUTke/IKUMvXcoidpuqd4Ii";

// Bu bir salt oluşturuyor, baştaki ilk argüman kaç seferde salt oluşturacağını söylüyor.
// ilk değer ne kadar büyük olursa salt oluşturması o kadar uzun sürüyor. Bu sayının büyük
// olması brute force saldırılarının önüne geçmeye yarıyor çünkü sayı büyüdükçe cevap süresi uzuyor.
bcrypt.genSalt(10, (err, salt) => {
  bcrypt.hash(password, salt, (err, hash) => {
    // burada hashlenmiş değeri veritabanına yazabiliriz.
    hashedPassword = hash;
    console.log(password,  hash);
  });
});

bcrypt.compare(password, hashedPassword, (err, res) => {
  console.log(res);
});
