"use strict";

const { MongoClient, ObjectID } = require("mongodb");

MongoClient.connect("mongodb://localhost:27017/TodoApp", (err, db) => {
  if (err){
    console.log("MongoDb veri tabanına bağlanılamadı. ", err.toString());
    return;
  }
  console.log("MongoDb veri tabanına bağlantı sağlandı.");

  // deleteMany : koşula uyan tüm kayıtları siler.
  db.collection("Todos").deleteMany({text : "Yap bir güzellik"})
    .then((result) => {
      console.log(result);
    });

  // deleteOne : Sadece kritere uyan ilk kaydı siler.

  db.collection("Todos").deleteOne({text : "Köpeği gezdir"})
    .then((result) => {
      console.log(result);
    });

  // findOneAndDelete : Kritere uyan ilk kaydı bulur result içinde döndürür ve siler.

  db.collection("Todos").findOneAndDelete({completed : false})
    .then((result) => {
      console.log(result);
    });

  db.collection("Users").findOneAndDelete({_id : new ObjectID("5ace1716214b23372ca2937a")})
    .then((result) => {
      if (result.lastErrorObject.n > 0) {
        console.log("Kullanıcı başarıyla silindi.");
      } else {
        console.log("Aradığınız kullanıcı mevcut değil.");
      }
    });

  // db.close();
});
